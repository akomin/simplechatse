package client.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import client.model.Client;
import client.model.ClientConfig;
import client.model.Message;

public class ClientMessageInputThread implements Runnable {
	private Message msg;
	@Override
	public void run() {

			BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String inputText;
				try {
					inputText = buffer.readLine();
					msg = new Message(ClientConfig.LOGIN, inputText);
					Client.messagesList.add(msg);
				} catch (IOException e) {

					e.printStackTrace();
				}
			}


	}

}
