/**
 * 
 */
package client.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * @author immortal
 *
 */
public class ClientConfig {
	
	private static final String PROPERTIES_FILE = "src/client/client.properties";
	public static int PORT;
	public static String IP_ADRESS;
	public static String LOGIN;
	public static String HELLO_MESSAGE;
	public static String USER_DISCONNECTED_MESSAGE;
	public static int HISTORY_LENGTH;
	
	static {
		Properties properties = new Properties();
		FileInputStream propertiesFile = null;
		
		try {
			propertiesFile = new FileInputStream(PROPERTIES_FILE);
			properties.load(propertiesFile);
			
			PORT = Integer.parseInt(properties.getProperty("PORT"));
			IP_ADRESS = properties.getProperty("IP_ADRESS");
			LOGIN = properties.getProperty("LOGIN");
			
			HISTORY_LENGTH  			 = Integer.parseInt(properties.getProperty("HISTORY_LENGTH"));
            HELLO_MESSAGE   			 = properties.getProperty("HELLO_MESSAGE");
            USER_DISCONNECTED_MESSAGE    = properties.getProperty("USER_DISCONNECTED_MESSAGE");
			
		} catch (FileNotFoundException e) {
			System.err.println("Properties file not found!");
			e.printStackTrace();
		}  catch (IOException e) {
			System.err.println("Error occured while reading file!");
			e.printStackTrace();
		
		} finally {
			try {
				propertiesFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
