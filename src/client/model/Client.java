package client.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Observable;


public class Client extends Observable implements Runnable {
	Socket clientSocket;
	Thread clientThread;
	private final String MESSAGE_RESEIVED = "Message received!";
	private final String MESSAGE_SENT = "Message sent!";
	protected static LinkedBlockingQueue<Message> inputMessagesList = new LinkedBlockingQueue<Message>(); 
	protected static LinkedBlockingQueue<Message> outputMessagesList = new LinkedBlockingQueue<Message>();
	protected ObjectInputStream ois;
	protected ObjectOutputStream oos;
	
	
	
	public Client() {
		
		
	}
	
	public void connect() {
		try {
			clientSocket = new Socket(ClientConfig.IP_ADRESS,ClientConfig.PORT);
			//Thread messageInput = new Thread(new ClientMessageInputThread());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}
		
		try {
			ois = (ObjectInputStream) clientSocket.getInputStream();
			oos = (ObjectOutputStream) clientSocket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		 clientThread = new Thread(this);
		 clientThread.start();
		 
		 Thread clientThreadSender = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					if (outputMessagesList.size() > 0) {
						Message message = outputMessagesList.poll();
						if (message != null) {
							try {
								oos.writeObject(message);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						break;
					}
				}
				
			}
			 
		 });

	}
	
	public void disconnect()  {
		try {
			this.clientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
	}

	@Override
	public void run() {
		Thread currentThread = Thread.currentThread();
		
		while (currentThread == clientThread) {
			
			try {
				Message message = (Message)ois.readObject();
				if (message != null) {
					inputMessagesList.add(message);
					this.notifyObservers(MESSAGE_RESEIVED);
				}else {
					break;
				}
						
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	/**
	 * @param args
	 */
//	public static void main(String[] args) {
//		
//		try {
//			Socket ClientSocket = new Socket(ClientConfig.IP_ADRESS,ClientConfig.PORT);
//			Thread messageInput = new Thread(new ClientMessageInputThread());
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			
//		}
//	}

}
