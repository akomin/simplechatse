/**
 * 
 */
package client.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author immortal
 *
 */
public class Message implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String login;
	private String message;
	private Date time;
	
	public Message(String login, String message) {
		this.login   = login;
		this.message = message;
		this.time    = new Date();
		
	}
	
	
	public String getLogin(){
		return this.login;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public String getDate() {
		return  this.time.toString(); 
	}
}
