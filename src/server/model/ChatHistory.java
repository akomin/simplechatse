package server.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Message> history;
	
	public ChatHistory() {
		this.history = new ArrayList<Message>(ServerConfig.HISTORY_LENGTH);
	}
	
	public void addMessage(Message message) {
		
		if (this.history.size() > ServerConfig.HISTORY_LENGTH) {
			this.history.remove(0);
		}
		
		this.history.add(message);
		
	}
	
	public List<Message> getHistory() {
		
		return this.history;
	}
}

