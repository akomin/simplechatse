package server.model;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    private Socket socket;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    public Client(Socket socket){
        this.socket = socket;
    }

    public Client(Socket socket , ObjectOutputStream oos , ObjectInputStream ois ){
        this.socket = socket;
        this.oos = oos;
        this.ois = ois;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public ObjectOutputStream getClientOutputStream() {
        return this.oos;
    }

    public ObjectInputStream getClientInputStream() {
        return this.ois;
    }

    public void setClientOutputStream(ObjectOutputStream oos) {
        this.oos = oos;
    }

    public void setClientInputStream(ObjectInputStream ois) {
        this.ois = ois;
    }
}
