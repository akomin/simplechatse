/**
 * 
 */
package server.model;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author immortal
 *
 */
public class Server {
	
	private static UserList userList = new UserList();
	private static ChatHistory chatHistory = new ChatHistory();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			ServerSocket socketListener = new ServerSocket(ServerConfig.PORT);
			
			while (true) {
				Socket client = null;
				
				while (client == null) {
					client = socketListener.accept();
				}
				new ServerClientThread(client);
			}
		} catch (IOException e) {
			System.err.println("Socket exception");
			e.printStackTrace();
		}

	}
	
	public synchronized static UserList getUserList() {
		return userList;
	}
	
	public synchronized static ChatHistory getChatHistory() {
		return chatHistory;
	}

}
